package com.gitlab.edufuga.wordlines.unitwriter;

import com.gitlab.edufuga.wordlines.core.Status;
import com.gitlab.edufuga.wordlines.core.WordStatusDate;
import com.gitlab.edufuga.wordlines.recordreader.FileSystemRecordReader;
import com.gitlab.edufuga.wordlines.recordreader.RecordReader;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UnitWriter {
    private final Path unitsFolder;
    private final RecordReader recordReader;

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public UnitWriter(Path unitsFolder, RecordReader recordReader) {
        this.unitsFolder = unitsFolder;
        this.recordReader = recordReader;
    }

    public UnitWriter(String unitsFolder, RecordReader recordReader) {
        this(Paths.get(unitsFolder), recordReader);
    }

    public void write(String fromWord, String toWord) {
        // from -> to
        try {
            // See if the from or to words exist already in the status folder.
            WordStatusDate from = recordReader.readRecord(fromWord);
            WordStatusDate to = recordReader.readRecord(toWord);

            if (from.getStatus() == null) {
                System.out.println("From word '"+fromWord+"' has no record. We won't write any connection for it.");
                return;
            }

            if (to.getStatus() == null) {
                System.out.println("To word '"+toWord+"' has no record. We won't write any connection pointing to it.");
                return;
            }

            // The records are present, but they could have undesired status for the connections.

            if (from.getStatus().equals(Status.UNKNOWN)) {
                System.out.println("From word '"+fromWord+"' is unknown. We won't write any connection for it.");
                return;
            }

            if (to.getStatus().equals(Status.UNKNOWN)) {
                System.out.println("To word '"+toWord+"' is unknown. We won't write any connection pointing to it.");
                return;
            }


            if (from.getStatus().equals(Status.IGNORED)) {
                System.out.println("From word '"+fromWord+"' is ignored. We won't write any connection for it.");
                return;
            }

            if (to.getStatus().equals(Status.IGNORED)) {
                System.out.println("To word '"+toWord+"' is ignored. We won't write any connection pointing to it.");
                return;
            }

            // Exclude words with the same contents.
            if (to.getWord().equalsIgnoreCase(from.getWord())) {
                System.out.println("From word and to word have the same contents. We won't write any connection.");
                return;
            }

            // Now that both "from" and "to" words exist as records, we can write a connection between them in form of a
            // "union". The union denotes the semantic sameness of the two things. This is for example useful for
            // denoting plurals of two words that the user knows without effort, so that he sees the two words as more
            // or less "same" (i.e. forming a union). Another example could be obvious verb forms, adjectives, etc. The
            // user has to decide explicitly what he or she considers a union.

            // Nested file structure.
            Path unit = unitsFolder
                    .resolve(from.getWord())
                    .resolve(to.getWord())
                    .resolve("unit.tsv");

            if (Files.notExists(unit)) {
                Files.createDirectories(unit.getParent());
            }

            Date date = new Date();

            String union = from.getWord() + "\t" + to.getWord() + "\t" + dateFormat.format(date) + "\n";

            Files.write(unit, union.getBytes());
        }
        catch(Exception e)
        {
            System.out.println("Nope.");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 3) {
            System.out.println("Usage: Units folder, 'from' and 'to' words.");
            return;
        }

        String units = args[0];
        String from = args[1];
        String to = args[2];

        Path unitsFolder = Paths.get(units);
        RecordReader recordReader = new FileSystemRecordReader(units);

        UnitWriter unitWriter = new UnitWriter(unitsFolder, recordReader);
        unitWriter.write(from, to);
   }
}
